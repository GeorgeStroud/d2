create database D2

use D2

create table client(

username varchar(20)not null,
password varchar(20),
fname varchar(20),
sname varchar(20),
email varchar(20),
phone varchar(20),
name varchar(20),
Id int,
primary key (username),
Foreign Key (name) references type,
Foreign Key (Id) references document
)

create table document(
Id int not null,
date int,
link varchar(20),
type varchar(20),
primary key (Id),
)

create table type(
name varchar(20) not null,
cost int,
hours int,
primary key(name)
)

create table appointment(
Aid int not null,
notes varchar(50),
username varchar(20),
Id int,
primary key (username),
Foreign Key (username) references client,
Foreign Key (Id) references timeslot
)

create table timeslot(
Id int not null,
time time not null,
date date not null,
username varchar(20),
primary key(Id)
)


create table instructor(
username varchar(20) not null,
password varchar(20),
fname varchar(20),
sname varchar(20),
email varchar(20),
phone int,
primary key(username)
)

create table car(
licence varchar(20) not null,
make varchar(20),
primary key (licence),
)

create table assigned(
confirmed varchar(20),
username varchar(20) not null,
licence varchar(20) not null,
Foreign key (username) references instructor,
Foreign key (licence) references car
)

create table admin(
username varchar(20) not null,
password varchar(20),
fname varchar(20),
sname varchar(20),
email varchar(20),
primary key(username)
)

insert into document values (1,'','','Learner Licence','')
insert into document values (2,'','','Advanced Certificate','')



insert into type values ('Leaners', '50', '1')
insert into type values ('Advanced', '100', '1')



insert into timeslot values (1, '07:00','2017-05-01')
insert into timeslot values (2, '08:00','2017-05-01')
insert into timeslot values (3, '09:00:00','2017-05-01')
insert into timeslot values (4, '10:00:00','2017-05-01')
insert into timeslot values (5, '11:00:00','2017-05-01')
insert into timeslot values (6, '12:00:00','2017-05-01')
insert into timeslot values (7, '13:00:00','2017-05-01')
insert into timeslot values (8, '14:00:00','2017-05-01')
insert into timeslot values (9, '15:00:00','2017-05-01')
insert into timeslot values (10,'16:00:00','2017-05-01')
insert into timeslot values (12, '17:00:00','2017-05-01')
insert into timeslot values (13, '18:00:00','2017-05-01')
insert into timeslot values (14, '19:00:00','2017-05-01')
insert into timeslot values (15, '20:00:00','2017-05-01')

insert into timeslot values (16, '07:00','2017-05-02')
insert into timeslot values (17, '08:00','2017-05-02')
insert into timeslot values (18, '09:00:00','2017-05-02')
insert into timeslot values (19, '10:00:00','2017-05-02')
insert into timeslot values (20, '11:00:00','2017-05-02')
insert into timeslot values (21, '12:00:00','2017-05-02')
insert into timeslot values (22, '13:00:00','2017-05-02')
insert into timeslot values (23, '14:00:00','2017-05-02')
insert into timeslot values (24, '15:00:00','2017-05-02')
insert into timeslot values (25, '16:00:00','2017-05-02')
insert into timeslot values (26, '17:00:00','2017-05-02')
insert into timeslot values (27, '18:00:00','2017-05-02')
insert into timeslot values (28, '19:00:00','2017-05-02')
insert into timeslot values (29, '20:00:00','2017-05-02')

insert into timeslot values (30, '07:00','2017-05-03')
insert into timeslot values (31, '08:00','2017-05-03')
insert into timeslot values (32, '09:00:00','2017-05-03')
insert into timeslot values (33, '10:00:00','2017-05-03')
insert into timeslot values (34, '11:00:00','2017-05-03')
insert into timeslot values (35, '12:00:00','2017-05-03')
insert into timeslot values (36, '13:00:00','2017-05-03')
insert into timeslot values (37, '14:00:00','2017-05-03')
insert into timeslot values (38, '15:00:00','2017-05-03')
insert into timeslot values (39, '16:00:00','2017-05-03')
insert into timeslot values (40, '17:00:00','2017-05-03')
insert into timeslot values (41, '18:00:00','2017-05-03')
insert into timeslot values (42, '19:00:00','2017-05-03')
insert into timeslot values (43, '20:00:00','2017-05-03')

insert into timeslot values (44, '07:00','2017-05-04')
insert into timeslot values (45, '08:00','2017-05-04')
insert into timeslot values (46, '09:00:00','2017-05-04')
insert into timeslot values (47, '10:00:00','2017-05-04')
insert into timeslot values (48, '11:00:00','2017-05-04')
insert into timeslot values (49, '12:00:00','2017-05-04')
insert into timeslot values (50, '13:00:00','2017-05-04')
insert into timeslot values (51, '14:00:00','2017-05-04')
insert into timeslot values (52, '15:00:00','2017-05-04')
insert into timeslot values (53, '16:00:00','2017-05-04')
insert into timeslot values (54, '17:00:00','2017-05-04')
insert into timeslot values (55, '18:00:00','2017-05-04')
insert into timeslot values (56, '19:00:00','2017-05-04')
insert into timeslot values (57, '20:00:00','2017-05-04')

insert into timeslot values (58, '07:00','2017-05-05')
insert into timeslot values (59, '08:00','2017-05-05')
insert into timeslot values (60, '09:00:00','2017-05-05')
insert into timeslot values (61, '10:00:00','2017-05-05')
insert into timeslot values (62, '11:00:00','2017-05-05')
insert into timeslot values (63, '12:00:00','2017-05-05')
insert into timeslot values (64, '13:00:00','2017-05-05')
insert into timeslot values (65, '14:00:00','2017-05-05')
insert into timeslot values (66, '15:00:00','2017-05-05')
insert into timeslot values (67, '16:00:00','2017-05-05')
insert into timeslot values (68, '17:00:00','2017-05-05')
insert into timeslot values (69, '18:00:00','2017-05-05')
insert into timeslot values (70, '19:00:00','2017-05-05')
insert into timeslot values (71, '20:00:00','2017-05-05')

insert into timeslot values (72, '07:00','2017-05-06')
insert into timeslot values (73, '08:00','2017-05-06')
insert into timeslot values (74, '09:00:00','2017-05-06')
insert into timeslot values (75, '10:00:00','2017-05-06')
insert into timeslot values (76, '11:00:00','2017-05-06')
insert into timeslot values (77, '12:00:00','2017-05-06')
insert into timeslot values (78, '13:00:00','2017-05-06')
insert into timeslot values (79, '14:00:00','2017-05-06')
insert into timeslot values (80, '15:00:00','2017-05-06')
insert into timeslot values (81, '16:00:00','2017-05-06')
insert into timeslot values (82, '17:00:00','2017-05-06')
insert into timeslot values (83, '18:00:00','2017-05-06')
insert into timeslot values (84, '19:00:00','2017-05-06')
insert into timeslot values (85, '20:00:00','2017-05-06')

insert into instructor values ('inst1', 'hogan', 'Hulk', 'Hogan','Hulk@gmail.com', 123456789)
insert into instructor values ('inst2', 'taylor', 'Taylor', 'Swift','taylor@gmail.com', 08001234)
insert into instructor values ('inst3', 'grant', 'Carey', 'Grant','Carey@gmail.com', 080089889)
insert into instructor values ('inst4', 'east', 'Clint', 'Eastwood','clint@gmail.com', 080015987)

insert into car values  ('ABC123','Toyota Corolla')
insert into car values  ('ABC456','Toyota Corolla')
insert into car values  ('ABC789','Toyota Corolla')
insert into car values  ('DEF123','Toyota Corolla')
insert into car values  ('DEF456','Toyota Corolla')

insert into client values ('test','t2', 't','2', 't2@gmail.com', '5000')





insert into appointment values ('1','')
insert into appointment values ('2', '')
insert into appointment values ('3', '')
insert into appointment values ('4', '')
insert into appointment values ('5', '')
insert into appointment values ('6', '')
insert into appointment values ('7', '')
insert into appointment values ('8', '')
insert into appointment values ('9', '')
insert into appointment values ('10', '')
insert into appointment values ('11', '')

insert into admin values ('admin1', 'poppy123', 'Bob', 'Smith', 'bobsmith@gmail.com' ) 
insert into admin values ('admin2', 'sunflower123', 'Dick', 'Miller', 'bobsmith@gmail.com' )
insert into admin values ('admin3', 'sweetpea123', 'Harry', 'Cooper', 'bobsmith@gmail.com' )


select * from timeslot

select * from document
select * from type
select * from appointment
select * from car
select * from admin
select * from instructor
select * from client

drop table client
drop table document
drop table type
drop table appointment
drop table timeslot
drop table car
drop table instructor
drop table admin
drop database D2